.. _Analysis:


==================================
Analysis
==================================

----------------------------------
Convergence plots
----------------------------------

You can use the tool called `amp-plotconvergence` to help you examine the output of an Amp log file. Run `amp-plotconvergence -h` for help at the command line.

You can also access this tool as `plot_convergence` from the `amp.analysis` module.

   .. image:: _static/convergence.svg
      :scale: 100 %
      :align: center

----------------------------------
Other plots
----------------------------------

There are several other plotting tools within this module, including ones for making parity plots and for examining the sensitivity of the model output to the model parameters.
See the module autodocumentation for details.

